# Simple single-page application (SPA) using Vue.js and Vue router

There are a lot more configuration files involved here. These are mostly boilerplate code provided by the Vue cli and are not interesting. The important stuff takes place in ```App.vue``` (where we have the master template and introduce the router-view component, that will be populated by the pages) and in ```index.js``` (where we load the individual page components and define the client-side routes). 

Each vue component inside the ```views``` folder houses one view and any possible JavaScript (and optionally CSS) related to that view. The contents of the ```<template>``` make up the markup that will be inserted into the ```<router-view/>```. In addition to a template tag, ```.vue``` files can also contain a ```<style>``` tag for CSS related to that specific view and a ```<script>``` tag for JavaScript code related to this view. You could, for example, load data from the server there.

Notice, how every view is loaded and rendered entirely on the client-side. This means, that the views change immediately without performing a full-page refresh. This allows for a more app-like user experience. The downside of SPAs is that with out server-rendered contents, SEO and sharing can be more difficult, more work is required from the client device, and initial page load can take more time.

Importantly, SPAs usually communicate with the backend systems via APIs, often REST and JSON.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
